					Date: 23 AUG 2014
Start time - 3:30 PM
Meeting at Workspace in Vindhya block, all the team members were present.
3:30 � 4:15 
Started to fill the Project Concept Doc
Discussed about the description of the project and profile of the users. 
 In Description we wrote about the project overview (What it is, its functionalities, etc)
In the profile of users, we discussed about the target users of the app, so that we could have a clear idea about the features.
4:15 � 5:00
Discussed about the UI of the app
Discussed about the features that we had to include into the app and how the different pages are to arranged .
We had some conflict about the timing schedule in the app. So we discussed about it for some time and have arrived at a solution .
To clear certain doubts we called our clients over phone, and clarified them.
5:00-5:20
Designed the whole structure on a sheet of paper (about the features and vague idea on  UI was also mentioned). The image was also attached with the Project concept Doc

End time : 5:30

